insert into room (id, price_per_night, room_type) values (1, 100, 'SINGLE');
insert into room (id, price_per_night, room_type) values (2, 100, 'DOUBLE');
insert into room (id, price_per_night, room_type) values (3, 100, 'SINGLE');
insert into room (id, price_per_night, room_type) values (4, 100, 'DOUBLE');
insert into room (id, price_per_night, room_type) values (5, 100, 'SINGLE');
insert into room (id, price_per_night, room_type) values (6, 100, 'TWIN');
insert into room (id, price_per_night, room_type) values (7, 100, 'SINGLE');
insert into room (id, price_per_night, room_type) values (8, 100, 'SINGLE');
insert into room (id, price_per_night, room_type) values (9, 100, 'STUDIO');
insert into room (id, price_per_night, room_type) values (10, 100, 'SINGLE');

insert into customer (id, name, address, email, customer_type) values (1, 'Pete', 'Tartu', 'abc@gmail.com', 'USER');
insert into customer (id, name, address, email, customer_type) values (2, 'John', 'Tartu', 'ac@gmail.com', 'USER');
insert into customer (id, name, address, email, customer_type) values (3, 'ABC', 'Tartu', 'abc1@gmail.com', 'AGENCY');


insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (1, '2016-03-22', '2015-03-22', 'CREATED', 1, 1)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (2, '2016-03-22', '2015-03-22', 'PENDING', 2, 2)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (3, '2016-03-22', '2015-03-22', 'CREATED', 1, 3)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (4, '2016-03-22', '2015-03-22', 'REJECTED', 3, 8)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (5, '2016-03-22', '2015-03-22', 'CREATED', 1, 6)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (6, '2016-03-22', '2015-03-22', 'CREATED', 1, 10)

