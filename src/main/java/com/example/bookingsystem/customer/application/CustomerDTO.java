package com.example.bookingsystem.customer.application;

import com.example.bookingsystem.customer.domain.model.Customer;
import com.example.bookingsystem.customer.domain.model.CustomerType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class CustomerDTO extends ResourceSupport{

	String _id;
	String name;

	String address;

	String email;

	CustomerType customerType;

	public Customer asUser(){
		return Customer.of(_id, name, address, email, customerType);
	}

}
