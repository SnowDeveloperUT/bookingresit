package com.example.bookingsystem.customer.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Repository
public interface CustomerRepository extends JpaRepository{}
