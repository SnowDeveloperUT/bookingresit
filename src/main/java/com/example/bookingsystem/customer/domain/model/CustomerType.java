package com.example.bookingsystem.customer.domain.model;

/**
 * Created by snowwhite on 6/20/2017.
 */
public enum CustomerType {

	AGENCY, USER
}
