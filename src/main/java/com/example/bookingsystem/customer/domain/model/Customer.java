package com.example.bookingsystem.customer.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Customer {

	@Id
	String id;

	String name;

	String address;

	String email;

	@Enumerated(EnumType.STRING)
	CustomerType customerType;

}
