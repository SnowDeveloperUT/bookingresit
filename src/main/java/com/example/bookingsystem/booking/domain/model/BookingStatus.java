package com.example.bookingsystem.booking.domain.model;

/**
 * Created by snowwhite on 6/20/2017.
 */
public enum BookingStatus {
	CREATED, PENDING, OPEN, REJECTED, CLOSED, CANCELLED
}
