package com.example.bookingsystem.booking.application.dto;

import com.example.bookingsystem.booking.domain.model.BookingStatus;
import com.example.bookingsystem.common.application.dto.BusinessPeriodDTO;
import com.example.bookingsystem.customer.application.CustomerDTO;
import com.example.bookingsystem.room.application.dto.RoomDTO;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Data
public class BookingDTO extends ResourceSupport{

	String _id;

	CustomerDTO customerDTO;

	RoomDTO roomDTO;

	BusinessPeriodDTO bookingDates;

	BookingStatus bookingStatus;

}
