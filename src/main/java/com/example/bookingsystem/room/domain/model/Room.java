package com.example.bookingsystem.room.domain.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Entity
@Getter
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Room {

	@Id
	String id;

	BigDecimal pricePerNight;

	@Enumerated(EnumType.STRING)
	RoomType roomType;
}
