package com.example.bookingsystem.room.domain.repository;

import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Repository
public interface RoomRepository extends CustomRoomRepository{}
