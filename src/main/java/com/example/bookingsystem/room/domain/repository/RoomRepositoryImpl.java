package com.example.bookingsystem.room.domain.repository;

import com.example.bookingsystem.room.domain.model.Room;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/20/2017.
 */
public class RoomRepositoryImpl implements CustomRoomRepository {

	@Autowired
	EntityManager em;

	@Override
	public List<Room> findAvailable(LocalDate startDate, LocalDate endDate) {
		return em.createQuery(
				"select i from Room where i not in (select r.property from Booking r where r.bookingStatus = 'ACCEPTED' and ?1 < r.bookingDates.endDate and ?2 > r.bookingDates.startDate)"
				, Room.class)
				.setParameter(1, startDate)
				.setParameter(2, endDate)
				.getResultList();
	}

	@Override
	public boolean isRoomAvailable(String id, LocalDate startDate, LocalDate endDate) {
		return em.createQuery("select case when count(*) > 0 then true else false end " +
						"from Room i where i.id = ?1 and i not in (select r.room from Booking r where r.bookingStatus = 'ACCEPTED' and ?2 < r.bookingDates.endDate and ?3 > r.bookingDates.startDate)"
				, Boolean.class)
				.setParameter(1, id )
				.setParameter(2, startDate)
				.setParameter(3, endDate)
				.getSingleResult();
	}
}
