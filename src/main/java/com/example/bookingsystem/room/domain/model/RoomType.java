package com.example.bookingsystem.room.domain.model;

/**
 * Created by snowwhite on 6/20/2017.
 */
public enum RoomType {
	SINGLE, DOUBLE, TWIN, STUDIO
}
