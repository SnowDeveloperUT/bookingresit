package com.example.bookingsystem.room.application.dto;

import com.example.bookingsystem.room.domain.model.Room;
import com.example.bookingsystem.room.domain.model.RoomType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/20/2017.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class RoomDTO extends ResourceSupport{

	String _id;
	BigDecimal pricePerNight;
	RoomType roomType;

	public Room asProperty(){
		return Room.of(_id, pricePerNight, roomType );
	}

}
